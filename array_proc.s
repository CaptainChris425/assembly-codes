# array_proc.s  
#
# This program uses a subroutine that computes the sum of elements of an array of integers.
# 
 


		.data
Sum_str:	.asciiz "Sum of the array elements: "
		
Array:		.word 5, 10, 15, 20, 25, 30
ArrayEnd:	.space 1

# Constants
PRINT_INT_SERV = 1
PRINT_STR_SERV = 4
TERMINATE_SERV = 10


	.text
	.globl  main
main:	
	la $a0, Array		# load base address of array into register $a0
	la $a1, ArrayEnd
	sub $a1, $a1, $a0	# 
	sra $a1, $a1, 2		# pass count of elements in array in $a1


	# Before call to calc_sum,
	# $a0 holds array address, $a1 holds array element count

	jal calc_sum		# return value is in $v0

	move $a2, $v0		# temporarily store $v0 for later use
	la $a0, Sum_str		# print summary string
	li $v0, PRINT_STR_SERV
	syscall


	move $a0, $a2		# put returned sum in $a0
	li $v0, PRINT_INT_SERV	# print an int
	syscall 

	li $v0, TERMINATE_SERV  # terminate program
     	syscall
.end main


# Prototype: int calc_sum(int * array, int count);
# Parameters:
#   array -- address of an array, passed in $a0
#   count -- no. of elements in array, passed in $a1
# Returns the sum of the elements of the array in $v0
# Registers used:
#  $s0 - current array offset
#  $s1 - an array element
#  $t0 - loop counter
#  $t1 - array sum
# $s0 and $s1 must be saved

calc_sum:

	addi $sp, $sp, -8	# Push $s0 and $s1
	sw $s0, ($sp)
	sw $s1, 4($sp)

	move $t0, $0		# Initialize loop counter
	move $t1, $0		# Initialize array sum
	move $s0, $a0		# Copy array start address to $s0
	
Loop:	lw $s1, ($s0)		# load a value from the array
	add $t1, $t1, $s1	# update sum
	addi $s0, $s0, 4	# update address to next word in array
	addi $t0, $t0, 1	# increment loop counter
	blt $t0, $a1, Loop

	
	move $v0, $t1		# copy sum to $v0 for return to main
	
	lw $s0, ($sp)		# Restore stack
	lw $s1, 4($sp)
	addi $sp, $sp, 8	
	jr $ra			# return to main
	







 








