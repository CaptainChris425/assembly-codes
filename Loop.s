# Loop.s  
#
# A simple program that uses a loop to display the first 10
# positive integers.
# 

        .data    
# Constants
NL  = '\n'		# Newline
MAX = 10

# System services
PRINT_INT_SERV  = 1
TERMINATE_SERV  = 10
PRINT_CHAR_SERV = 11


        .text
        .globl  main
main:

	li $t0, 1	# Loop control variable
Loop:	
	# Print the integer.
	li $v0, PRINT_INT_SERV
	move $a0, $t0
	syscall

	# Print a newline
	li $v0, PRINT_CHAR_SERV
	li $a0, NL
	syscall

	# Increment the loop counter
	addi $t0, $t0, 1

	# Do it all again if $t0 <= MAX
	ble $t0, MAX, Loop 


	# All done
	li $v0, TERMINATE_SERV
	syscall
.end main
  

        



