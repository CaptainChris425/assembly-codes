# fp_conversions.s  
#
# Perform several operations on a mix of integer and float/double value.
# 

        .data
int_val:	.word 23
float_val:	.float 6.1
double_val:	.double 12.3
double_val2:	.double 6.1
Prompt1: 	.asciiz "The product is: " 
Prompt2:	.asciiz "The sum (float + double) is: "  
Prompt3:	.asciiz "The sum (double + double) is: "                    

# Constants

NL  = '\n'		# Newline


PRINT_INT_SERV    = 1
PRINT_FLOAT_SERV  = 2
PRINT_DOUBLE_SERV = 3 
PRINT_STR_SERV    = 4
TERMINATE_SERV    = 10
PRINT_CHAR_SERV   = 11


        .text
        .globl  main
main:

        # -------------------------------------------------
	# 1. Multiply an integer and a double.
	# -------------------------------------------------
	# Load the values to be multiplied.
	lw $t0, int_val
	l.d $f0, double_val

	# First, we have to move the integer to a double register.
	mtc1.d $t0, $f2

	# Next, we have to convert the value to double form.
	# (The number in $f0 is still in integer form.)
	
	cvt.d.w $f2, $f2

	# We're now ready to do the multiplication.
	mul.d $f12, $f0, $f2

	# Print a message
	la $a0, Prompt1
	li $v0, PRINT_STR_SERV
	syscall

	# Print the result
	li $v0, PRINT_DOUBLE_SERV
	syscall

	# Print a newline
	li $a0, '\n'
	li $v0, PRINT_CHAR_SERV
	syscall

        # -------------------------------------------------
	# 2. Add a float and a double.
	# -------------------------------------------------
	# Load the float value to be added (the double is still in $f0/$f1.)
	l.s $f2, float_val
	
	# float_val is in a FP register, but we still have to 
	#   convert it to a double.
	# If you don't do the conversion, you get the wrong answer!
	cvt.d.s $f2, $f2

	# Now add the two numbers.
	add.d $f12, $f0, $f2

	# Print a message
	la $a0, Prompt2
	li $v0, PRINT_STR_SERV
	syscall

	# Print the result
	li $v0, PRINT_DOUBLE_SERV
	syscall

	# Print a newline
	li $a0, '\n'
	li $v0, PRINT_CHAR_SERV
	syscall

	# For a comparison, add the same two numbers, 
	# but with both as doubles.
	l.d $f2, double_val2

	add.d $f12, $f0, $f2

	# Print a message
	la $a0, Prompt3
	li $v0, PRINT_STR_SERV
	syscall

	# Print the result
	li $v0, PRINT_DOUBLE_SERV
	syscall
        
        
	li $v0, TERMINATE_SERV
	syscall

.end main

