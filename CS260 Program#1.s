#Christopher Young
#Program Assignment #1 Date: 10-3-2017
#This program sums up an array and outputs the sum as well as the largest number in the array
		


	.globl main
		.data
#Constants
newline = '\n'
sumis: .asciiz "Sum is: "	#to print sum is
maxis: .asciiz "Max is: "	#to print max is

# System service constants
PRINT_INT_SERV = 1
PRINT_CHAR_SERV = 11
TERMINATE_SERV = 10
PRINT_STR_SERV  = 4
	  	
array:    	.word   5, 10 , 15, 20, 25, 28, 24, 18, 14, 3
arrayEnd: 	.space 1

		.text
main:
	
	la $t1, arrayEnd	#store end of array
	la $t0, array		#stores location of array
	subu $t0, $t1, $t0	#find the differenced is bytes between array($t0) and array end($t1)
	srl $t0, $t0, 2		#shift over two bytes 
				#same as dividing by 4
				#$t0 = number of elements in array
	add $t1, $0, $0		#$t1 = loop counter
	add $t2, $0, $0		#$t2 = offset varible to move
				#through the array
	add $t3, $0,$0		#starts sum at 0
	add $s0, $0, $0		#starts the max at 0

Loop:
	bge $t1, $t0, Done	#if loop counter is bigger than
				#elements in array then branch to Done(Finishing loop)
	lw $t4, array($t2)	#load the element in array at position ($t2)
	add $t3, $t3, $t4	#stores sum in $t3
	bge $t4,$s0,Max		#if current element is greater to max, branch to Max
	b Else
Max:
	move $s0, $t4		#store max in $s0
Else:
	addiu $t1, $t1, 1	# Increment loop counter
	addiu $t2, $t2, 4	# Increment offset to get to the next word
	b Loop
Done:
	la $a0, maxis		#Print out "Max is: "
	li $v0, PRINT_STR_SERV
	syscall
	move $a0, $s0		#Prints out the max value
	li $v0, PRINT_INT_SERV
	syscall
	li $a0, newline		# Print a newline char
	li $v0, PRINT_CHAR_SERV 
	syscall
	la $a0, sumis		#Print out "Sum is: "
	li $v0, PRINT_STR_SERV
	syscall
	move $a0, $t3		#Print out the value of the sum
	li $v0, PRINT_INT_SERV
	syscall
	li $v0, TERMINATE_SERV  # terminate program
     	syscall
	