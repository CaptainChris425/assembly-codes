	.data
NL = '\n'
MAX = 100
FIZZstr:	.asciiz "FIZZ"
BUZZstr:	.asciiz "BUZZ"
PRINT_INT_SERV  = 1
PRINT_STR_SERV  = 4
READ_INT_SERV	= 5
TERMINATE_SERV  = 10
PRINT_CHAR_SERV = 11

	.text
	.globl main
main:
	li $t0, 0
	li $t1, 0
Loop:
	addi $t0,$t0,1
	rem $t1,$t0,3
	beq $t1,0,FIZZ
	rem $t1,$t0,5
	beq $t1,0,BUZZ
	b Done
BUZZ:
	la $a0, BUZZstr
	li $v0, PRINT_STR_SERV
	syscall
	li $a0, NL
	li $v0, PRINT_CHAR_SERV
	syscall
	b Done
FIZZ:
	la $a0, FIZZstr
	li $v0, PRINT_STR_SERV
	syscall
	rem $t1,$t0,5
	beq $t1,0,BUZZ
	li $a0, NL
	li $v0, PRINT_CHAR_SERV
	syscall
	b Done

Done:	
	ble $t0, 99, Loop
	li $v0, TERMINATE_SERV
	syscall
.end main 