# Prog1Soln.s  
#
# For a given array of integers in memory,
# a) find and display the sum of the array elements, and 
# b) find and display the largest value of the array.


# Constants
NL = '\n'

# System service constants
PRINT_INT_SERV = 1
PRINT_STR_SERV = 4
PRINT_CHAR_SERV = 11
TERMINATE_SERV = 10 


		.data
Array:    	.word   5, 10 , 15, 20, 25, 28, 24, 18, 14, 3
ArrayEnd: 	.space 1 
SumMsg:		.asciiz "Sum of the array elements: "
LargestMsg:	.asciiz "Largest array value: "

		.text
        	.globl  main
main:	
	# Register usage
	# $t0 - Number of array elements
	# $t1 - Loop counter
	# $t2 - Offset in array
	# $s0 - Sum accumulator
	# $s1 - Largest value

	# Count the number of array elements
	la $t1, ArrayEnd
	la $t0, Array
	lw $s1, ($t0)		# Store first array element in $s1
				# So far, it's the largest

	subu $t0, $t1, $t0	# Difference in addresses, in bytes
	srl $t0, $t0, 2		# Equivalent to $t0 = $t0/4
				# $t0 = Number of words in array

	add $t1, $0, $0		# Initialize loop counter
	add $t2, $0, $0		# Initialize relative offset in array

	li $s0, 0		# Sum of array elements	


	# Iterate through the array	
Loop:
	beq $t1, $t0, Done
	lw $a0, Array($t2)	# Load the array element at offset $t2

	# Accumulate it onto the sum
	add $s0, $s0, $a0

	# See if current array element is larger
	ble $a0, $s1, LessEqual

	# $a0 must be larger than $s1, so make $a0 the new max value
	move $s1, $a0

LessEqual:
	addiu $t1, $t1, 1	# Increment loop counter
	addiu $t2, $t2, 4	# Increment offset to get to the next word
	b Loop
	
Done:	
	# Display array prompt
	la $a0, SumMsg
	li $v0, PRINT_STR_SERV
	syscall

	# ... and value
	move $a0, $s0
	li $v0, PRINT_INT_SERV
	syscall

	li $a0, NL		# Print a newline char
	li $v0, PRINT_CHAR_SERV 
	syscall

	# Display largest value prompt
	la $a0, LargestMsg
	li $v0, PRINT_STR_SERV
	syscall

	# ... and value
	move $a0, $s1
	li $v0, PRINT_INT_SERV
	syscall
	
.end main

	


	

	li $v0, TERMINATE_SERV  # terminate program
     	syscall
.end main

	









