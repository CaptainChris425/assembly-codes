# odd_even.s  
#
# Take input of an integer value from the user, and report whether 
# that number is odd or even.
# The program demonstrates basic "if - else" logic.

	.data
# Constants

PRINT_INT_SERV  = 1
PRINT_STR_SERV  = 4
READ_INT_SERV	= 5
TERMINATE_SERV  = 10



promptStr:	.asciiz "Enter an integer: "
reportStr: 	.asciiz " is "
oddStr:		.asciiz "odd."
evenStr:	.asciiz "even."



        .text
        .globl  main
main:
	#######################################
	# Prompt the user to enter an int.
	#######################################
	la $a0, promptStr
	li $v0, PRINT_STR_SERV
	syscall

	#######################################
	# Input an int.
	#######################################
	li $v0, READ_INT_SERV
	syscall		# After syscall, the value read will be in $v0

	move $t0, $v0	# Save the input value

	#######################################
	# Print the first part of the prompt, echoing the input value.
	#######################################
	move $a0, $t0
	li $v0,PRINT_INT_SERV
	syscall
 
	la $a0, reportStr
	li $v0, PRINT_STR_SERV
	syscall

	#######################################
	# Determine whether the number is odd or even.
	#######################################
	andi $t1, $t0, 1	# Get the least significant bit. 


	# If $t1 == 1, the input number was odd; if $t1 == 0, the number was even.
	beq $t1, 1, is_odd

	# To get here, the number was even.
	la $a0, evenStr
	li $v0, PRINT_STR_SERV
	syscall
	b Done

is_odd:  
	# The number was odd.
	la $a0, oddStr
	li $v0, PRINT_STR_SERV
	syscall

Done:

	li $v0, TERMINATE_SERV		# exit
	syscall

.end main




