# ptr_array.s - Work with a "two-dimensional array"
#   that is really an array of addresses.
# This program will print the i-th entry of each row.
# In other words, pretending that Array is a two-dimensional array,
#   print the elements on the main diagonal. 

# Constants
NUM_ROWS	= 5
NL		= '\n'

PRINT_INT_SERV  = 1
PRINT_STR_SERV  = 4
TERMINATE_SERV	= 10
PRINT_CHAR_SERV = 11


	.data
row0:	.word 1,  2,  3,  4,  5
row1:	.word 2,  4,  6,  8, 10
row2:	.word 3,  6,  9, 12, 15
row3: 	.word 4,  8, 12, 16, 20
row4:	.word 5, 10, 15, 20, 25

Array:	.word row0, row1, row2, row3, row4	# The addresses of the row_n arrays
					
stride:	.word row1 - row0	# Length in bytes of each row -- not used

	.text	
main:	
	la $s0, Array		# Base address
	li $t0, 0		# Loop counter
	li $t1, 0		# Byte address within a row
	
Loop:
	# Notice that in the next three lines,we are dereferencing twice:
	#   first, to get the right row, and
	#   second, to get the right element of that row
	
	lw $s1, ($s0)		# Load the address of the current row from Array
	addu $s1, $s1, $t1	# Add the offset of the current element in that row
	lw $a0, ($s1)		# Get the value of that element from that row

	# Print the value
	li $v0, PRINT_INT_SERV
	syscall

	# Print a newline
	li $a0, NL
	li $v0, PRINT_CHAR_SERV
	syscall

	# Increments
	addi $t0, $t0, 1	# Increment loop counter
	addu $s0, $s0, 4	# Increment $s0 to next element in Array
	addi $t1, $t1, 4	# Increment $t1 to next element in row_n
	bne $t0, NUM_ROWS, Loop
	
	la $v0, TERMINATE_SERV	
	syscall
.end main

