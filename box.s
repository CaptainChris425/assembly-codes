# box.s  
#
# Calculate the volume and surface area of a box with user-supplied (integer) dimensions.
# 

        .data
widPrompt:	.asciiz "Enter the width of the box ... " 
htPrompt:	.asciiz "Enter the height of the box ... "  
depPrompt: 	.asciiz "Enter the depth of the box ... "
volPrompt:	.asciiz "The volume is ... "
surfPrompt:	.asciiz "The total surface area is ... "

width:		.word 0
height:		.word 0
depth:		.word 0
vol:		.word 0
surfaceA:	.word 0
                   


# Constants
prompt = '>'		# Input prompt
nl  = '\n'		# Newline

PRINT_INT_SERV  = 1
PRINT_STR_SERV  = 4
READ_INT_SERV   = 5
TERMINATE_SERV  = 10
PRINT_CHAR_SERV = 11


        .text
        .globl  main
main:
        
 
        # Prompt for the width
	la $a0, widPrompt
	li $v0, PRINT_STR_SERV
	syscall

	# Input the width
	li $v0, READ_INT_SERV
	syscall
	sw $v0, width

        # Prompt for the height
	la $a0, htPrompt
	li $v0, PRINT_STR_SERV
	syscall

	# Input the height
	li $v0, READ_INT_SERV
	syscall
	sw $v0, height

        # Prompt for the depth
	la $a0, depPrompt
	li $v0, PRINT_STR_SERV
	syscall

	# Input the depth
	li $v0, READ_INT_SERV
	syscall
	sw $v0, depth

	# Set up parameters for volume subroutine
	lw $a0, width
	lw $a1, height
	lw $a2, depth

	# Call volume subroutine
	jal volume

	sw $v0, vol

	# Display the volume
	la $a0, volPrompt
	li $v0, PRINT_STR_SERV
	syscall

	lw $a0, vol
	li $v0, PRINT_INT_SERV
	syscall

	# Print a new-line char
	li $a0, nl
	li $v0, PRINT_CHAR_SERV
	syscall

	# Set up parameters for surfArea subroutine
	# Only $a0 needs to be set, since 
	#   height and depth are still in $a1 and $a2
	lw $a0, width		 

	# Call surfArea subroutine
	jal surfArea

	sw $v0, surfaceA

	# Display the surface area
	la $a0, surfPrompt
	li $v0, PRINT_STR_SERV
	syscall

	lw $a0, surfaceA
	li $v0, PRINT_INT_SERV
	syscall
        

        li      $v0, TERMINATE_SERV        
        syscall
.end main


# volume - Calculate and return the volume of a box with the specified dimensions.
# Prototype: int volume(int wid, int ht, int depth);
# Parameters:
#   $a0 - width
#   $a1 - height
#   $a2 - depth
# Returns the volume in $v0
# 
volume:

	mul $v0, $a0, $a1	# $v0 = width * height
	mul $v0, $v0, $a2	# $v0 = $v0 * depth
	jr $ra

# surfArea - Calculate and return the surface area of a box with the specified dimensions.
# Prototype: int surfArea(int wid, int ht, int depth);
# Parameters:
#   $a0 - width
#   $a1 - height
#   $a2 - depth
# Returns the surface area in $v0
# 
surfArea:
	# Save $s0
	addi $sp, $sp, -4
	sw $s0, ($sp)
	mul $s0, $a0, $a1	# $t0 = width * height
	add $v0, $s0, $0	# $v0 = width * height

	mul $s0, $a0, $a2	# $t0 = width * depth
	add $v0, $v0, $s0	# $v0 = width * height + width * depth

	mul $s0, $a1, $a2	# $t0 = height * depth
	add $v0, $v0, $s0	# $v0 = height * depth + width * height + width * depth
	
	sll $v0, $v0, 1		# Multiply $v0 by 2

	# Restore $s0
	lw $s0, ($sp)
	addi $sp, $sp, 4
	jr $ra





